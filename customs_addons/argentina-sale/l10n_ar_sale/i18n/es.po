# Translation of Odoo Server.
# This file contains the translation of the following modules:
# * l10n_ar_sale
# 
# Translators:
# Juan José Scarafía <scarafia.juanjose@gmail.com>, 2018
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server 11.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-08-23 19:53+0000\n"
"PO-Revision-Date: 2018-08-23 19:53+0000\n"
"Last-Translator: Juan José Scarafía <scarafia.juanjose@gmail.com>, 2018\n"
"Language-Team: Spanish (https://www.transifex.com/adhoc/teams/46451/es/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Language: es\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. module: l10n_ar_sale
#: model:ir.model.fields,field_description:l10n_ar_sale.field_sale_order_line_report_price_subtotal
msgid "Amount"
msgstr "Subtotal"

#. module: l10n_ar_sale
#: model:ir.model,name:l10n_ar_sale.model_res_company
msgid "Companies"
msgstr "Compañías"

#. module: l10n_ar_sale
#: code:addons/l10n_ar_sale/models/sale_order_line.py:118
#, python-format
msgid ""
"Debe haber un y solo un impuestos de IVA por lÃ­nea. Verificar lÃ­neas con "
"producto \"%s\""
msgstr ""

#. module: l10n_ar_sale
#: model:ir.model.fields,help:l10n_ar_sale.field_res_company_sale_allow_vat_no_discrimination
#: model:ir.model.fields,help:l10n_ar_sale.field_res_config_settings_sale_allow_vat_no_discrimination
msgid ""
"Definie behaviour on sales orders and quoatations reports. Discrimination or not will depend in partner and company responsability and AFIP letters setup:\n"
"* If No, Always Discriminate, then VAT will be discriminated like always in odoo\n"
"* If Yes, No Discriminate Default, if no match found it won't discriminate by default\n"
"* If Yes, Discriminate Default, if no match found it would discriminate by default"
msgstr ""

#. module: l10n_ar_sale
#: model:ir.model,name:l10n_ar_sale.model_account_invoice_report
msgid "Invoices Statistics"
msgstr "Estadisticas de Facturas"

#. module: l10n_ar_sale
#: model:ir.ui.view,arch_db:l10n_ar_sale.res_config_settings_view_form
msgid "Line subtotals in sales orders"
msgstr ""

#. module: l10n_ar_sale
#: model:ir.model.fields,field_description:l10n_ar_sale.field_sale_order_line_report_price_net
msgid "Net Amount"
msgstr "Importe Neto"

#. module: l10n_ar_sale
#: selection:res.company,sale_allow_vat_no_discrimination:0
msgid "No, Always Discriminate"
msgstr ""

#. module: l10n_ar_sale
#: code:addons/l10n_ar_sale/models/sale_order_line.py:52
#, python-format
msgid "Only one vat tax allowed per line"
msgstr ""

#. module: l10n_ar_sale
#: model:ir.model.fields,field_description:l10n_ar_sale.field_sale_order_line_price_unit_with_tax
msgid "Price Unit Price"
msgstr "Precio Unitario"

#. module: l10n_ar_sale
#: model:ir.model,name:l10n_ar_sale.model_sale_order
msgid "Quotation"
msgstr ""

#. module: l10n_ar_sale
#: model:ir.model.fields,field_description:l10n_ar_sale.field_res_company_sale_allow_vat_no_discrimination
#: model:ir.model.fields,field_description:l10n_ar_sale.field_res_config_settings_sale_allow_vat_no_discrimination
msgid "Sale Allow VAT no discrimination?"
msgstr "Permitir no discriminar IVA en ventas?"

#. module: l10n_ar_sale
#: model:res.groups,name:l10n_ar_sale.sale_price_unit_with_tax
msgid "Sale Unit Prices w/ Taxes"
msgstr "Precio Unitario con Impuestos"

#. module: l10n_ar_sale
#: model:ir.model,name:l10n_ar_sale.model_sale_order_line
msgid "Sales Order Line"
msgstr "Línea de orden de ventas"

#. module: l10n_ar_sale
#: model:ir.ui.view,arch_db:l10n_ar_sale.res_config_settings_view_form
msgid "Show Unit Price with Taxes included On sale Order Lines"
msgstr ""

#. module: l10n_ar_sale
#: model:ir.model.fields,field_description:l10n_ar_sale.field_sale_order_report_amount_tax
msgid "Tax"
msgstr "Impuesto"

#. module: l10n_ar_sale
#: model:ir.model.fields,field_description:l10n_ar_sale.field_sale_order_line_report_tax_id
msgid "Taxes"
msgstr "Impuestos"

#. module: l10n_ar_sale
#: model:ir.model.fields,field_description:l10n_ar_sale.field_sale_order_line_report_price_unit
msgid "Unit Price"
msgstr "Precio Unitario"

#. module: l10n_ar_sale
#: model:ir.model.fields,field_description:l10n_ar_sale.field_res_config_settings_group_price_unit_with_tax
msgid "Unit Price w/ Taxes"
msgstr ""

#. module: l10n_ar_sale
#: model:ir.model.fields,field_description:l10n_ar_sale.field_sale_order_report_amount_untaxed
msgid "Untaxed Amount"
msgstr "Subtotal s/ Impuestos"

#. module: l10n_ar_sale
#: model:ir.model.fields,field_description:l10n_ar_sale.field_sale_order_vat_discriminated
msgid "Vat Discriminated"
msgstr ""

#. module: l10n_ar_sale
#: model:ir.model.fields,field_description:l10n_ar_sale.field_sale_order_line_vat_tax_id
msgid "Vat Tax"
msgstr ""

#. module: l10n_ar_sale
#: selection:res.company,sale_allow_vat_no_discrimination:0
msgid "Yes, Discriminate Default"
msgstr ""

#. module: l10n_ar_sale
#: selection:res.company,sale_allow_vat_no_discrimination:0
msgid "Yes, No Discriminate Default"
msgstr "Si, No discriminar por defecto"

#. module: l10n_ar_sale
#: model:ir.model,name:l10n_ar_sale.model_res_config_settings
msgid "res.config.settings"
msgstr ""
