# -*- coding: utf-8 -*-

from odoo import models, fields, api

class webcuba_deploy(models.Model):
     _name = 'webcuba_deploy.webcuba_deploy'

     url = fields.Char(string="Repository url")
     branch = fields.Char(string="Git branch")
     target_dir = fields.Char(string="Deploy directory")
     keygit_dir = fields.Char(string="SSH key directory")
     #script_path = fields.Char(string="Deploy script path")

#     value = fields.Integer()
#     value2 = fields.Float(compute="_value_pc", store=True)
#     description = fields.Text()
#
#     @api.depends('value')
#     def _value_pc(self):
#         self.value2 = float(self.value) / 100