# -*- coding: utf-8 -*-
from odoo import http
from odoo.tools.osutil import listdir
import sh
import os



class Deploy(http.Controller):

    @http.route('/webcuba_deploy/deploy/', auth='public')

    def deploy(self, token):

        if token == '3SxJJE1fNsm5Pf4B6Wwx':


            deploys = http.request.env['webcuba_deploy.webcuba_deploy']
            dep = deploys.search([])[0]

            RUTA_PROYECTO = os.path.dirname(os.path.realpath(__file__)).replace('controllers','deploy_webcuba.sh')

            path = str(dep.keygit_dir)

            if not path.endswith('/'):
                path += '/'

            try:
                sh.sh(RUTA_PROYECTO, dep.url, dep.branch, dep.target_dir, path, _out="/tmp/dir_contents")
                with open("/tmp/dir_contents", "r") as h:
                    texto = h.readlines()
                h.close()
            except:
                return('<h1>Error running the deploy script </h1>')

            return http.request.render('webcuba_deploy.deploy', {'teachers': texto,
            })
        else:
            return('<h1>Access Deny </h1>')

    # @http.route('/webcuba_deploy/webcuba_deploy/', auth='public')
    # def index(self, **kw):
    #     return "Hello, world"

#     @http.route('/webcuba_deploy/webcuba_deploy/objects/', auth='public')
#     def list(self, **kw):
#         return http.request.render('webcuba_deploy.listing', {
#             'root': '/webcuba_deploy/webcuba_deploy',
#             'objects': http.request.env['webcuba_deploy.webcuba_deploy'].search([]),
#         })

#     @http.route('/webcuba_deploy/webcuba_deploy/objects/<model("webcuba_deploy.webcuba_deploy"):obj>/', auth='public')
#     def object(self, obj, **kw):
#         return http.request.render('webcuba_deploy.object', {
#             'object': obj
#         })
