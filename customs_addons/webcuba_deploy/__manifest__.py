# -*- coding: utf-8 -*-
{
    'name': "webcuba_deploy",

    'summary': """
        Deploy github odoo module
	""",

    'description': """
        This module permit to config github projects for deploy in servers very easy
    """,

    'author': "Webcuba",
    'website': "http://www.webcuba.es",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Developer',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','website'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/deploy.xml',
        'views/templates.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}
