#!/bin/bash

remote_repository=$1
branch=$2
target_dir=$3
keygit_dir=$4
tmp_dir=/tmp/webcuba_deploy/
version_file=${tmp_dir}VERSION
clean_up=false

set -e

echo "Checking the environment..."
echo ""
echo "Running as $(whoami)"
echo ""

for program in "git" "rsync"; do
        if ! which $program; then
                    echo "${program} not available. It needs to be installed on the server for this script to work."
                            exit 1
        fi
done


echo ""
echo "Environment OK."
echo ""
echo "Deploying ${remote_repository} ${branch}"
echo "to ${target_dir} ..."
echo ""

if [ ! -d "$tmp_dir"  ]; then
    # Clone the repository into the TMP_DIR
    echo $ GIT_SSH=${keygit_dir}ssh_wrapper git clone --depth=1 --branch $branch $remote_repository $tmp_dir 
    GIT_SSH=${keygit_dir}ssh_wrapper git clone --depth=1 --branch $branch $remote_repository $tmp_dir
else
    # TMP_DIR exists and hopefully already contains the correct remote origin
    # so we'll fetch the changes and reset the contents.
    echo $ GIT_SSH=${keygit_dir}ssh_wrapper git --git-dir="${tmp_dir}.git" --work-tree="${tmp_dir}" fetch origin $branch 
    GIT_SSH=${keygit_dir}ssh_wrapper git --git-dir="${tmp_dir}/.git" --work-tree="${tmp_dir}" fetch origin $branch
    echo ""
    echo $ GIT_SSH=${keygit_dir}ssh_wrapper git --git-dir="${tmp_dir}.git" --work-tree="${tmp_dir}" reset --hard FETCH_HEAD 
    GIT_SSH=${keygit_dir}ssh_wrapper git --git-dir="${tmp_dir}/.git" --work-tree="${tmp_dir}" reset --hard FETCH_HEAD
    echo ""
fi


cd $tmp_dir

# Update the submodules
echo $ GIT_SSH=${keygit_dir}ssh_wrapper git submodule update --init --recursive 
GIT_SSH=${keygit_dir}ssh_wrapper git submodule update --init --recursive
echo ""

# Describe the deployed version
echo $ GIT_SSH=${keygit_dir}ssh_wrapper git --git-dir="${tmp_dir}.git" --work-tree="${tmp_dir}" describe --always > ${version_file} 
GIT_SSH=${keygit_dir}ssh_wrapper git --git-dir="${tmp_dir}.git" --work-tree="${tmp_dir}" describe --always > ${version_file}
echo ""

# Deployment command
echo $ rsync -rltgoDzvO $tmp_dir $target_dir --exclude=.git 
rsync -rltgoDzvO $tmp_dir $target_dir --exclude=.git
echo ""

# Remove the TMP_DIR (depends on CLEAN_UP)
if [ $clean_up = true ]; then
	echo $ rm -rf $tmp_dir
	rm -rf $tmp_dir
	echo ""
fi

echo "Done"

